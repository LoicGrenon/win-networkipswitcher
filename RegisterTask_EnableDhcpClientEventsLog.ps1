<#PSScriptInfo
.VERSION 1.0
.AUTHOR Loïc GRENON <loic.grenon@difoolou.net>
#>

<#
.DESCRIPTION
Create a Scheduled Task on an event to activate the DHCP-Client events logging
(Microsoft-Windows-DHCP-Client/Operational).
This task is triggered at each network connection (ethernet cable plugging, WiFi activation, etc ...)
using Microsoft-Windows-NetworkProfile/Operational events log and EventID 10000.
#>

# When we connect to a network, an event ID 10000 is registered on Microsoft-Windows-NetworkProfile/Operational
# So we use it as a task trigger
$class = cimclass MSFT_TaskEventTrigger root/Microsoft/Windows/TaskScheduler
$Trigger = $class | New-CimInstance -ClientOnly
$Trigger.Enabled = $true
$Trigger.Subscription = '<QueryList><Query Id="0" Path="Microsoft-Windows-NetworkProfile/Operational">
                         <Select Path="Microsoft-Windows-NetworkProfile/Operational">' +
                        '*[System[Provider[@Name=''Microsoft-Windows-NetworkProfile''] and EventID=10000]]' +
                        '</Select></Query></QueryList>'

# Call a script to activate the DHCP-Client events logging (Microsoft-Windows-DHCP-Client/Operational)
$ActionParameters = @{
    Execute  = 'C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe'
    Argument = '-NoProfile -File C:\scripts\NetworkIpSwitcher\EnableDhcpClientEventLog.ps1'
}
$Action = New-ScheduledTaskAction @ActionParameters

# Security context in which the task is run
$Principal = New-ScheduledTaskPrincipal -UserId 'NT AUTHORITY\SYSTEM' -LogonType ServiceAccount

# Configuration that the Task Scheduler service uses to determine how to run the task
$SettingsParameters = @{
    AllowStartIfOnBatteries    = $true
    DontStopIfGoingOnBatteries = $true
    ExecutionTimeLimit         = New-TimeSpan -Hours 1
}
$Settings = New-ScheduledTaskSettingsSet @SettingsParameters

# Registers the task definition on the tasks scheduler
$RegSchTaskParameters = @{
    TaskName    = 'Enable DHCP-client events logger'
    Description = 'runs at network connection (ethernet cable plugging, WiFi activation, etc ...) '+
                  'and execute a script to enable the DHCP-Client events logger ' +
                  '(Microsoft-Windows-DHCP-Client/Operational)'
    TaskPath    = '\NetworkIpSwitcher\'
    Action      = $Action
    Principal   = $Principal
    Settings    = $Settings
    Trigger     = $Trigger
}
Register-ScheduledTask @RegSchTaskParameters
