<#PSScriptInfo
.VERSION 1.0
.AUTHOR Loïc GRENON <loic.grenon@difoolou.net>
#>

<#
.DESCRIPTION

This script is called at each cable connection using TaskScheduler
"\NetworkIpSwitcher\Activate DHCP"
See RegisterTask_DhcpClient_onMediaConnection.ps1
#>

$adapter = Get-NetAdapter -Physical -InterfaceDescription "*Ethernet*" | ? {$_.Status -eq "up"}
$interface = $adapter | Get-NetIPInterface -AddressFamily "IPv4"
If ($interface.Dhcp -eq "Disabled") {
    # Remove existing gateway
    If (($interface | Get-NetIPConfiguration).Ipv4DefaultGateway) {
        $interface | Remove-NetRoute -Confirm:$false
    }
    # Enable DHCP
    $interface | Set-NetIPInterface -DHCP Enabled
    # Configure the DNS Servers automatically
    $interface | Set-DnsClientServerAddress -ResetServerAddresses
}
