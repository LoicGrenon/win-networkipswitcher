<#PSScriptInfo
.VERSION 1.0
.AUTHOR Loïc GRENON <loic.grenon@difoolou.net>
#>

<#
.DESCRIPTION

This script is called when no DHCP server is available using TaskScheduler
"\NetworkIpSwitcher\Set static IP addresses"
See RegisterTask_SetStaticIP_onDhcpFails.ps1
#>

# Retrieve the physical ethernet adapter to configure
$adapter = Get-NetAdapter -Physical -InterfaceDescription "*Ethernet*" | ? {$_.Status -eq "up"}
If (($adapter).count -eq 0) {
    Throw "No connected physical Ethernet adapter is found."
} ElseIf(($adapter).count -gt 1) {
    Throw "Too much connected physical Ethernet adapters are found."
}

# Get NIC id
$InterfaceId = $adapter | Select-Object -ExpandProperty ifIndex

# Remove any existing settings from the adapter
AdapterNetIpConfiguration = Get-NetIPConfiguration -InterfaceIndex $InterfaceId
If ($AdapterNetIpConfiguration.IPv4Address.IPAddress) {
    Remove-NetIPAddress -InterfaceIndex $InterfaceId -AddressFamily "IPv4" -Confirm:$false
}
If ($AdapterNetIpConfiguration.Ipv4DefaultGateway) {
    Remove-NetRoute -InterfaceIndex $InterfaceId -AddressFamily "IPv4" -Confirm:$false
}

# Function to convert subnet mask IP address to CIDR value
function Convert-IpAddressToMaskLength([string] $dottedIpAddressString) {
    $result = 0; 
    # ensure we have a valid IP address
    [IPAddress] $ip = $dottedIpAddressString;
    $octets = $ip.IPAddressToString.Split('.');
    foreach($octet in $octets) {
        while(0 -ne $octet) {
            $octet = ($octet -shl 1) -band [byte]::MaxValue
            $result++; 
        }
    }
    return $result;
}

$IP = "192.168.0.250"
$SubnetMask = "255.255.255.0"
$Gateway = ""

# Configure the new adapter settings
If ($Gateway -ne "") {
    $NetIpAddressParam = @{
        InterfaceIndex = $InterfaceId
        AddressFamily  = "IPv4"
        IPAddress      = $IP
        PrefixLength   = Convert-IpAddressToMaskLength $SubnetMask
        DefaultGateway = $Gateway
    }
} else {
    $NetIpAddressParam = @{
        InterfaceIndex = $InterfaceId
        AddressFamily  = "IPv4"
        IPAddress      = $IP
        PrefixLength   = Convert-IpAddressToMaskLength $SubnetMask
    }
}
New-NetIPAddress @NetIpAddressParam
# Set secondary IP addresses
New-NetIPAddress -InterfaceIndex $InterfaceId -IPAddress 192.168.10.250 -SkipAsSource $true
New-NetIPAddress -InterfaceIndex $InterfaceId -IPAddress 10.0.7.250 -PrefixLength 24 -SkipAsSource $true
