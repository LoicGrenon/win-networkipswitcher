<#PSScriptInfo
.VERSION 1.0
.AUTHOR Loïc GRENON <loic.grenon@difoolou.net>
#>

<#
.DESCRIPTION

#>

$InterfaceId = 0
# Get connected physical ethernet adapter
$adapter = Get-NetAdapter -Physical -InterfaceDescription "*Ethernet*" | ? {$_.Status -eq "up"}
$interfaceId = $adapter | Select-Object -ExpandProperty ifIndex

If ($InterfaceId -eq 0) {
    Throw  "No connected physical Ethernet adapter is found."
}

$class = cimclass MSFT_TaskEventTrigger root/Microsoft/Windows/TaskScheduler
$Trigger = $class | New-CimInstance -ClientOnly
$Trigger.Enabled = $true
$Trigger.Subscription = '<QueryList><Query Id="0" Path="Microsoft-Windows-DHCP-Client/Operational">
                         <Select Path="Microsoft-Windows-DHCP-Client/Operational">' +
                        '*[System[(EventID=50023)]] and *[EventData[(Data[@Name="InterfaceId"]=' + $($InterfaceId) + ')]]' +
                        '</Select></Query></QueryList>'

# Call a script 
$ActionParameters = @{
    Execute  = 'C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe'
    Argument = '-NoProfile -File C:\scripts\NetworkIpSwitcher\SetStaticIpAddresses.ps1'
}
$Action = New-ScheduledTaskAction @ActionParameters

# Security context in which the task is run
$Principal = New-ScheduledTaskPrincipal -UserId 'NT AUTHORITY\SYSTEM' -LogonType ServiceAccount

# Configuration that the Task Scheduler service uses to determine how to run the task
$SettingsParameters = @{
    AllowStartIfOnBatteries    = $true
    DontStopIfGoingOnBatteries = $true
    ExecutionTimeLimit         = New-TimeSpan -Hours 1
}
$Settings = New-ScheduledTaskSettingsSet @SettingsParameters

# Registers the task defintion on the tasks scheduler
$RegSchTaskParameters = @{
    TaskName    = 'Set static IP addresses'
    Description = 'Set static IP addresses when no DHCP server is available'
    TaskPath    = '\NetworkIpSwitcher\'
    Action      = $Action
    Principal   = $Principal
    Settings    = $Settings
    Trigger     = $Trigger
}
Register-ScheduledTask @RegSchTaskParameters
