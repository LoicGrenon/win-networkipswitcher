<#PSScriptInfo
.VERSION 1.0
.AUTHOR Loïc GRENON <loic.grenon@difoolou.net>
#>

<#
.DESCRIPTION
Activate DHCP-Client events logging (Microsoft-Windows-DHCP-Client/Operational)

This script is called at each network connection using TaskScheduler
"\NetworkIpSwitcher\Enable DHCP-client events logger"
See RegisterTask_EnableDhcpClientEventsLog.ps1
#>

try {
	$LogName = 'Microsoft-Windows-DHCP-Client/Operational'
	$EventLog = Get-WinEvent -ListLog $LogName
	$EventLog.set_IsEnabled($true)
	$EventLog.SaveChanges()
	Write-Verbose -Message "Successfully enabled log $LogName"
} catch {
	Write-Warning -Message "Failed to enable $LogName event logger because: $($_.Exception.Message)"
}
